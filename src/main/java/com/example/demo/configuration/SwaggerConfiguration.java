package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;


@Configuration
@OpenAPIDefinition
public class SwaggerConfiguration {
	  @Bean
	  public OpenAPI baseOpenApi() {
	    return new OpenAPI()
	        .info(new Info()
	            .title("Drink API")
	            .version("1.0")
	            .description("Develop for testing")
	            .contact(new Contact()
	                .email("chheaveangsreang@gmail.com")
	                .name(": Contact to Chheaveang"))
	            .termsOfService("TOS")
	            .license(new License()
	                .name("PPCBank")));
	  }
}
