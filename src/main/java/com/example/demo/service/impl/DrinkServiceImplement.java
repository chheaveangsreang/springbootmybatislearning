package com.example.demo.service.impl;

import java.util.List;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Drink;
import com.example.demo.model.DrinkRequest;
import com.example.demo.model.DrinkUpdateRequest;
import com.example.demo.respository.DrinkRepository;
import com.example.demo.service.DrinkService;

@Service
public class DrinkServiceImplement implements DrinkService{
	
	private final DrinkRepository drinkRepository;

	public DrinkServiceImplement(DrinkRepository drinkRepository) {
		this.drinkRepository = drinkRepository;
	}



	@Override
	public List<Drink> getAllDrinks(Integer page, Integer size) {
		// TODO Auto-generated method stub
		return drinkRepository.getAllDrinks(page, size);
	}



	@Override
	public Drink findDrinkById(Integer id) throws NotFoundException {
		// TODO Auto-generated method stub
		 Drink drink = drinkRepository.findDrinkById(id);
		 if(drink == null ) throw new NotFoundException("Cannot find drink with id "+id);
		 return drink;
		 
	}



	@Override
	public Integer addDrink(DrinkRequest drink) {
		// TODO Auto-generated method stub
		
		return drinkRepository.insert(drink);
		
	}



	@Override
	public void removeDrink(Integer id) {
		// TODO Auto-generated method stub
		drinkRepository.removeDrink(id);
		
	}



	@Override
	public void updateDrink(DrinkUpdateRequest drinkUpdateRequest) {
		// TODO Auto-generated method stub
		drinkRepository.updateDrink(drinkUpdateRequest);
		
	}

}
