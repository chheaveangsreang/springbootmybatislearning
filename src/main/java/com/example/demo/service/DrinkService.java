package com.example.demo.service;

import java.util.List;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Drink;
import com.example.demo.model.DrinkRequest;
import com.example.demo.model.DrinkUpdateRequest;

//@Service
public interface DrinkService {

	public List<Drink> getAllDrinks(Integer page, Integer size);

	public Drink findDrinkById(Integer id) throws NotFoundException;

	public Integer addDrink(DrinkRequest drink);

	public void removeDrink(Integer id);

	public void updateDrink(DrinkUpdateRequest drinkUpdateRequest);

}
