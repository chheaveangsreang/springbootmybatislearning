package com.example.demo.respository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.demo.model.Drink;
import com.example.demo.model.DrinkRequest;
import com.example.demo.model.DrinkUpdateRequest;

@Mapper
public interface DrinkRepository {

	@Select("SELECT * FROM Drink LIMIT #{size} OFFSET #{size}*(#{page}-1)")
	List<Drink> getAllDrinks(Integer page, Integer size);

	
	@Select("SELECT * FROM Drink WHERE id = #{id}")
	Drink findDrinkById(Integer id);


	@Select("INSERT INTO drink (name, price)"
			+ "VALUES (#{drink.name}, #{drink.price})"
			+ "RETURNING  id")
	Integer insert(@Param("drink") DrinkRequest drink);


	@Delete("DELETE FROM drink WHERE id = #{id}")
	void removeDrink(Integer id);


	@Update("UPDATE drink SET name = #{drink.name},"
			+ "price=#{drink.price}"
			+ "WHERE id = #{drink.id}")
	void updateDrink(@Param("drink") DrinkUpdateRequest drinkUpdateRequest);

}
