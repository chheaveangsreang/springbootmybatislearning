package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Drink;
import com.example.demo.model.DrinkRequest;
import com.example.demo.model.DrinkUpdateRequest;
import com.example.demo.model.response.Response;
import com.example.demo.service.DrinkService;

@RestController
@RequestMapping("/api/v1/drinks")
public class DrinkController {
	
	private final DrinkService drinkService;
	
	public DrinkController(DrinkService drinkService) {
		this.drinkService = drinkService;
	}





	@GetMapping
	public ResponseEntity<?> getAllDrinks(
				@RequestParam Integer page,
				@RequestParam Integer size
			){
		
		if(page<=0) throw new IllegalStateException("Page can not be less than 1");
		if(page<=0) throw new IllegalStateException("Size can not be less than 1");
	
		List<Drink> drink = drinkService.getAllDrinks(page,size);
		
		Response<List<Drink>> response = new Response<>();
		response.setStatus("Successfully");
		response.setMessage("successfully fetched drink");
		response.setTimestamp(LocalDateTime.now());
		response.setData(drink);
	
//		Response<List<Drink>> response = Response
//				.<List<Drink>>builder()
//				.message("Successfully fetch data")
//				.status("Success")
//				.build();
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(response);
		
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getDrinkById(@PathVariable Integer id) throws NotFoundException{
		Drink drink = drinkService.findDrinkById(id);
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(drink);
	}
	
	
	
	@PostMapping
	public ResponseEntity<?> insertNewDrink(
			@RequestBody DrinkRequest drinkRequest
			) throws NotFoundException{
		
		Integer id = drinkService.addDrink(drinkRequest);
		Drink drink = drinkService.findDrinkById(id);
		
		Response<Drink> response = new Response<>();
		response.setStatus("Success");
		response.setMessage("Successfully insert new drink");
		response.setData(drink);
		response.setTimestamp(LocalDateTime.now());
		
		
		return ResponseEntity.ok().body(response);
	}
	
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteDrink(@PathVariable Integer id){
		drinkService.removeDrink(id);
		
		Response<Drink> response = new Response<>();
		response.setStatus("Success");
		response.setMessage("Successfully delete drink with id "+id);
		response.setTimestamp(LocalDateTime.now());
		return ResponseEntity.ok().body(response);
	}
	
	
	@PutMapping
	public ResponseEntity<?> UpdateDrinkDetail(@RequestBody DrinkUpdateRequest drinkUpdateRequest) throws NotFoundException{
		
		drinkService.updateDrink(drinkUpdateRequest);
		
		Response<Drink> response = new Response<>();
		response.setStatus("Success");
		response.setMessage("Successfully update drink with id "+drinkUpdateRequest.getId());
		response.setData(drinkService.findDrinkById(drinkUpdateRequest.getId()));
		response.setTimestamp(LocalDateTime.now());
		return ResponseEntity.ok().body(response);
		
	}
	
	



}
